# !/usr/bin/env python
# -*- coding: utf-8 -*-
import time, random
from bs4 import BeautifulSoup
import requests
import datetime
import os
import urllib
import pypyodbc
import uuid

from connection_db import Connect


class SearchScript:

    def start(self):                    
        class_conn = Connect()
        conn_s = class_conn.connection()
        conn_f = class_conn.connection()
        conn_l = class_conn.connection()
        cursor_s = conn_s.cursor()
        cursor_f = conn_f.cursor()
        cursor_l = conn_l.cursor()

        cursor_l.execute("SELECT count(*) FROM spider WHERE isActivated = 1 and isGoogleRunning = 0") 
        check = cursor_l.fetchone()[0]
            
        if check > 0:

            print('Google searching ...')
            self.check_running(1)

            cursor_s.execute("SELECT * FROM import WHERE isPause = 0 and status_g = 0")
            
            for row in cursor_s.fetchall():   

                id_row = row[0]
                id_data = row[1]

                fullname = str( row[2] + ' ' + row[3] )
                data = '"' + fullname + '"' + ' ' + row[6]

                try:
                    self.main(data, id_data)
                    pass
                except:
                    pass

                self._pause() #Pause 30 sec.
                
                try:
                    change_status_process = "UPDATE import SET status_g = 1 WHERE id_search = '%s' and id = '%d'" % (id_data, id_row)
                    cursor_f.execute(change_status_process)
                    cursor_f.commit()
                except:
                    print("Error on DB")
                    pass
                pass
            
            self.check_running(0)
            
            conn_f.close()
            conn_l.close()
            conn_s.close()
        
        else:
            print('is Google running ...')
            exit()


    def main(self, query, id_search):

        url2 = "https://www.google.se/search?hl=sv&q=%s&tbas=0&tbs=qdr:y,sbd:1&source=lnt&sa=X"\
            "&ved=0ahUKEwisyI2st9vgAhUuHbkGHSTbAGkQpwUIIw&biw=1920&bih=966"

        print("\nSearch for: %s" % query)
        msg = 'Was did not possible to search by '
        
        try:
            self.choice_g(url2, query, id_search)
            pass
        except IOError:
            print(msg + 'Google\n')
            pass

        exit()


    def choice_g(self, url3, query, id_search):
        query = query.replace(' ', '+')
        string = url3 % query
        r = requests.get(string)

        arg = str(query)
        print("\n")
        print(string)
        self.execute_task3(r, arg, id_search)


    def execute_task3(self, r, query, id_search):
        soup = BeautifulSoup(r.text, 'lxml')

        id_result = uuid.uuid4()

        for item in soup.find('div', attrs={"id": "search"}):
            for itens in item.find_all('div', class_='g'):

                f = query.replace(" ", "_")
                f = f.replace('"', "")

                path0 = "_test/google/%s" % f
                path0 = path0.replace(",", "")
                path0 = path0.replace("__", "_")

                try:
                    os.makedirs(path0)
                except OSError:
                    pass


                title = str(itens.h3.a.text)
                url = str(itens.h3.a['href']).replace('/url?q=','')
                resume = str(itens.find('span', class_='st').text)

                r0 = requests.get(str(itens.h3.a['href']).replace('/url?q=',''))
                hunt = BeautifulSoup(r0.text, 'lxml')
                
                body = str(hunt.body)

                try:                    
                    self.saveDb_Data(id_result, title, url, resume, hunt.head, body)                                            
                    pass
                except:
                    self.saveDb_Data(id_result, title, url, resume, '', '')
                    self.save_html(hunt, path0)
                    pass
                               
                pass
            
            total = len(soup.find_all('div', class_='g'))
            pass
        print("Results: %s" % total)
        self.saveDb_Result(id_search, id_result, f, total, 'google')


    def _pause(self):
        wait = random.uniform(20, 30)
        print('%s seconds pause' % wait)
        time.sleep(wait)   

    def saveDb_Result(self, id_search, id_result, search_description, results, engine):
        
        class_conn = Connect()
        conn_r = class_conn.connection()

        e = None
        
        try:
            cursor_r = conn_r.cursor()
        
        except e:
            if e.__class__ == pypyodbc.ProgrammingError:
                cursor_r = conn_r.cursor()
        
        try:
            insert = "INSERT INTO result (id_search, id_result, search_description, results, engine)" \
            " VALUES('%s', '%s', '%s', '%s', '%s')" % (id_search, id_result, search_description, results, engine)
            cursor_r.execute(insert)
            cursor_r.commit()
            pass
        except e:
            print('Error on insert query - tbl : result_new\n' + e)
            pass        

        conn_r.close()

    def saveDb_Data(self, id_result, title, url, resume, meta, body):
        
        class_conn = Connect()
        conn = class_conn.connection()

        e = None
        
        try:
            cursor = conn.cursor()
        
        except e:
            if e.__class__ == pypyodbc.ProgrammingError:
                cursor = conn.cursor()
        
        try:
            insert = "INSERT INTO result_data (id_result, title, url, resume, meta, body) " \
            "VALUES('%s', '%s', '%s', '%s', '%s', '%s')" % (id_result, title, url, resume, meta, body)
            cursor.execute(insert)
            cursor.commit()
            pass
        except e:
            print('Error on insert query - tbl : result_data_new\n' + str(e))
            pass        
        

        conn.close()

    @staticmethod
    def check_running(num):

        class_conn = Connect()
        conn_c = class_conn.connection()
        cursor_c = conn_c.cursor()

        change_status_spider = "UPDATE spider SET isGoogleRunning = %d WHERE isActivated = 1" % num
        cursor_c.execute(change_status_spider)
        cursor_c.commit()

        conn_c.close()

    @staticmethod
    def save_html(soup, path_dir):
        timex = datetime.datetime.now()
        tmf = str("tm_" + timex.strftime("%I-%M-%S"))
        path = path_dir + "/results_at_time_" + tmf + ".html"

        try:
            file = open(path, "ab")
            file.write(soup.encode('utf-8'))
            file.close()
        except IOError:
            file = open(path, "wb")
            file.write(soup.encode('utf-8'))
            file.close()

    @staticmethod
    def save_log(search_for, results, engine):
        timex = datetime.datetime.now()
        tmf = str("tm_" + timex.strftime("%d-%m-%Y"))
        path = "_test/log_" + tmf + ".csv"

        for_ = search_for.replace(",", "_")
        for_ = for_.replace("__", "_")

        header = 'TimeStamp, Search for, Results, Engine;\n'
        data = 'Time: ' + str(timex.strftime("%I:%M:%S %p")) + ' , ' + for_ + ' , ' + str(results) + ',' + engine + \
               ';\n'

        if os.path.exists(path):
            try:
                file = open(path, "ab")
                file.write(data.encode())
                file.close()
            except IOError:
                file = open(path, "wb")
                file.write(data.encode())
                file.close()

            pass

        else:
            try:
                file = open(path, "ab")
                file.write(header.encode())
                file.write(data.encode())
                file.close()
            except IOError:
                file = open(path, "wb")
                file.write(header)
                file.write(data)
                file.close()

            pass


y = SearchScript()
y.start()