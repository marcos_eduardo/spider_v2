#### Instalation & Requirements

We use python 3.6 or 3.7.2 version for this project.
Install [lxml](http://lxml.de/installation.html), requests and simplejson libraries.
Install Microsoft Visual C++ Compiler for Python 3.7

Python download:
https://www.python.org/downloads/


If you haven't python, install the version 3.7 and execute this command: `python get-pip.py`

exec
`pip install -r requirements.txt`

After that, execute the commands above:
`pip install pypyodbc`


And execute this command : `python get-pip.py --user`

### Connection

Change the database string connection in "connection_db.py"

Create a database search, you can run the .sql script on "_INSTALL" folder.

### Usage

`go to directory of the application`

Run `python google.py or bing.py or yahoo.py`

***
After that, a log file will be created and will save all events, folder called logs!!